#include <stdio.h>

void swap(int * array, int i, int j) {
	int tmp = array[j];
	array[j] = array[i];
	array[i] = tmp;
}

void subBubbleSort(int * array, int length, int i, int j) {
	if (j >= length - i - 1) return;

	if (array[j + 1] < array[j]) {
		swap(array, j, j + 1);
	}

	subBubbleSort(array, length, i, j + 1);
}

void bubbleSort(int * array, int length, int i) {
	if (i >= length - 1) return;

	subBubbleSort(array, length, i, 0);
	bubbleSort(array, length, i + 1);
}

int main() {
	int array [] = {0,9,2,8,6,4,7,5,3,1};
	bubbleSort(array, 10, 0);

	printf("%d", array[0]);

	for (int i = 1; i < 10; ++i) {
		printf(",%d", array[i]);
	}

	printf("\n");
}
